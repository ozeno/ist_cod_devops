FROM nginx:alpine
RUN mkdir -p /usr/share/nginx/html/dist
COPY dist/ /usr/share/nginx/html/dist
COPY index.html /usr/share/nginx/html
EXPOSE 80
