const { openBrowser, goto, write, click } = require('taiko');

(async () => {
  try {
    await openBrowser({args: ['--no-sandbox', '--disable-setuid-sandbox']});
    await goto("google.co.uk");
    await write("taiko test automation");
    await click("Google Search");
  } catch (error) {
    console.error(error);
  } finally {
    closeBrowser();
  }
})();